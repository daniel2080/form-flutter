import 'package:flutter/material.dart';

class FormScreen extends StatefulWidget {
  @override
  _FormScreenState createState() => _FormScreenState();
}

class _FormScreenState extends State<FormScreen> {
  String _name;
  String _email;
  String _password;
  String _url;
  String _phoneNumber;
  String _calories;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Widget _buildName() {
    return TextFormField(
      decoration: InputDecoration(
        hintText: 'Enter a Name  Here',
        labelText: "Name",
      ),
      validator: (value) => value.isEmpty ? "Please write an Email" : null,
      onSaved: (newValue) => newValue = _name,
    );
  }

  Widget _buildEmail() {
    return TextFormField(
      decoration: InputDecoration(
        border: InputBorder.none,
        hintText: 'Enter an Email  Here',
        labelText: "Email",
      ),
      validator: (value) => value.isEmpty ? "Please write an Email" : null,
      onSaved: (newValue) => newValue = _email,
    );
  }

  Widget _buildPassword() {
    return TextFormField(
      decoration: InputDecoration(
        border: InputBorder.none,
        hintText: 'Enter Password',
        labelText: "Password",
      ),
      validator: (value) => value.isEmpty ? "Please write an Email" : null,
      onSaved: (newValue) => newValue = _password,
    );
  }

  Widget _buildUrl() {
    return TextFormField(
      decoration: InputDecoration(
        border: InputBorder.none,
        hintText: 'Enter an Url',
        labelText: "Url",
      ),
      validator: (value) => value.isEmpty ? "Please write an Url" : null,
      onSaved: (newValue) => newValue = _url,
    );
  }

  Widget _buildPhoneNumber() {
    return TextFormField(
      decoration: InputDecoration(
        border: InputBorder.none,
        hintText: 'Enter a PhoneNumber',
        labelText: "PhoneNumber",
      ),
      validator: (value) => value.isEmpty ? "Please write an Email" : null,
      onSaved: (newValue) => newValue = _phoneNumber,
    );
  }

  Widget _buildCalories() {
    return TextFormField(
      decoration: InputDecoration(
        border: InputBorder.none,
        hintText: 'Enter Your Colories',
        labelText: "Colories",
      ),
      validator: (String value) {
        if (value.isEmpty) {
          return 'Please enter some text';
        }
      },
      onSaved: (newValue) => newValue = _calories,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("FormDemo"),
        centerTitle: true,
      ),
      body: Container(
        margin: EdgeInsets.all(24.0),
        child: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                _buildName(),
                // _buildEmail(),
                // _buildPassword(),
                // _buildPhoneNumber(),
                // _buildUrl(),
                // _buildCalories(),
                SizedBox(height: 106.0),
                RaisedButton(
                  child: Text(
                    "Sumbit",
                    style: TextStyle(color: Colors.blue, fontSize: 16.0),
                  ),
                  onPressed: () => {
                    if (!_formKey.currentState.validate())
                      {}
                    else
                      {
                        _formKey.currentState.save(),
                        print(_name),
                      }
                  },
                ),
              ],
            )),
      ),
    );
  }
}
